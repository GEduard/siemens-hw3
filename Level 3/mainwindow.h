#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPlainTextEdit>
#include <qtabwidget.h>
#include <qlist.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
	QTabWidget *tabWidget;
	QPlainTextEdit *tBox;
	QList<QPlainTextEdit*> tList;
	QList<QString> fList;
	int tabCount;
    ~MainWindow();
public slots:
	void newFile();
	void openFile(bool);
	void saveAsFile(bool);
	void saveFile(bool);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
