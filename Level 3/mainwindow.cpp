#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPlainTextEdit>
#include <QVBoxLayout> 
#include <QFileDialog>
#include <qtextstream.h>
#include <qtabwidget.h>
#include <qfileinfo.h>
#include <qlist.h>
#include <qfile.h>
#include <fstream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
	
	ui->centralWidget->setWindowTitle("Notepad++");
	tabWidget = new QTabWidget(ui->centralWidget);
	tBox = new QPlainTextEdit(ui->centralWidget);
	QVBoxLayout *layout = new QVBoxLayout;
	layout->addWidget(tabWidget);
	tabWidget->addTab(tBox, "New1");
	ui->centralWidget->setLayout(layout);
	tList.append(tBox);
	fList.append(NULL);
	tabCount = 1;
	connect(ui->actionSave_As, SIGNAL(triggered(bool)), this, SLOT(saveAsFile(bool)));
	connect(ui->actionOpen, SIGNAL(triggered(bool)), this, SLOT(openFile(bool)));
	connect(ui->actionSave, SIGNAL(triggered(bool)), this, SLOT(saveFile(bool)));
	connect(ui->actionNew, SIGNAL(triggered(bool)), this, SLOT(newFile()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::newFile()
{
	tabCount++;
	tBox = new QPlainTextEdit(ui->centralWidget);
	QString s, tabName;
	tabName = QString("New");
	s = QString::number(tabCount);
	tabName.append(s);
	tabWidget->addTab(tBox, tabName);
	tList.append(tBox);
	fList.append(NULL);
}

void MainWindow::openFile(bool)
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Text files (*.txt)"));
	QFile file(fileName);
	file.open(QIODevice::ReadOnly);
	QFileInfo fileInfo(file.fileName());
	QTextStream in(&file);
	QString text = in.readAll();
	tBox = new QPlainTextEdit(ui->centralWidget);
	tBox->setPlainText(text);
	file.close();
	tList.append(tBox);
	fList.append(fileName);
	tabWidget->addTab(tBox, fileInfo.fileName());
}

void MainWindow::saveAsFile(bool)
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), "", tr("Text files (*.txt)"));
	QFile file(fileName);
	QFileInfo fileInfo(file.fileName());
	std::ofstream out(fileName.toStdString());
	out << tList.at(tabWidget->currentIndex())->toPlainText().toStdString();
	tabWidget->setTabText(tabWidget->currentIndex(), fileInfo.fileName());
	fList.append(fileName);
}

void MainWindow::saveFile(bool)
{
	if (fList.at(tabWidget->currentIndex()) == NULL)
		saveAsFile(true);
	else
	{
		QString fileName = fList.at(tabWidget->currentIndex());
		std::ofstream out(fileName.toStdString());
		out << tList.at(tabWidget->currentIndex())->toPlainText().toStdString();
	}
}
