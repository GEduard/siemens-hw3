/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QSlider *rSlider;
    QSlider *bSlider;
    QSlider *gSlider;
    QLabel *rLabel;
    QLabel *gLabel;
    QLabel *bLabel;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(400, 300);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        rSlider = new QSlider(centralWidget);
        rSlider->setObjectName(QStringLiteral("rSlider"));
        rSlider->setGeometry(QRect(90, 50, 191, 21));
        rSlider->setOrientation(Qt::Horizontal);
        bSlider = new QSlider(centralWidget);
        bSlider->setObjectName(QStringLiteral("bSlider"));
        bSlider->setGeometry(QRect(90, 150, 191, 21));
        bSlider->setOrientation(Qt::Horizontal);
        gSlider = new QSlider(centralWidget);
        gSlider->setObjectName(QStringLiteral("gSlider"));
        gSlider->setGeometry(QRect(90, 100, 191, 22));
        gSlider->setOrientation(Qt::Horizontal);
        rLabel = new QLabel(centralWidget);
        rLabel->setObjectName(QStringLiteral("rLabel"));
        rLabel->setGeometry(QRect(40, 50, 41, 21));
        gLabel = new QLabel(centralWidget);
        gLabel->setObjectName(QStringLiteral("gLabel"));
        gLabel->setGeometry(QRect(40, 100, 31, 21));
        bLabel = new QLabel(centralWidget);
        bLabel->setObjectName(QStringLiteral("bLabel"));
        bLabel->setGeometry(QRect(40, 150, 31, 20));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 400, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        rLabel->setText(QApplication::translate("MainWindow", "Red:", Q_NULLPTR));
        gLabel->setText(QApplication::translate("MainWindow", "Green:", Q_NULLPTR));
        bLabel->setText(QApplication::translate("MainWindow", "Blue:", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
