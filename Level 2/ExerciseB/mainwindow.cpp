#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qcolor.h>
#include <qpalette.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
	ui->centralWidget->setAutoFillBackground(true);

	ui->rSlider->setMinimum(0);
	ui->bSlider->setMinimum(0);
	ui->gSlider->setMinimum(0);
	ui->rSlider->setMaximum(255);
	ui->gSlider->setMaximum(255);
	ui->bSlider->setMaximum(255);
	connect(ui->rSlider, SIGNAL(valueChanged(int)), this, SLOT(sChanged()));
	connect(ui->gSlider, SIGNAL(valueChanged(int)), this, SLOT(sChanged()));
	connect(ui->bSlider, SIGNAL(valueChanged(int)), this, SLOT(sChanged()));
}

MainWindow::~MainWindow()
{

    delete ui;
}

void MainWindow::sChanged()
{
	QColor col = QColor(ui->rSlider->value(), ui->gSlider->value(), ui->bSlider->value(), 200);
	QPalette pal = QPalette(col);
	ui->centralWidget->setPalette(pal);
}
