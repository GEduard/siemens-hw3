#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

	connect(ui->copyButton, SIGNAL(clicked()), this, SLOT(clickedButton()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::clickedButton()
{
	ui->lineDst->setText(ui->lineSrc->text());
}
