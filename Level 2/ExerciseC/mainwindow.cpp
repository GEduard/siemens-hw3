#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qvboxlayout>
#include <qpushbutton.h>
#include <qlineedit.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
	QVBoxLayout *layout = new QVBoxLayout;
	ui->centralWidget->setLayout(layout);
	QPushButton *addLEdit = new QPushButton("Add a new line edit", this);
	layout->addWidget(addLEdit);
	connect(addLEdit, SIGNAL(clicked()), this, SLOT(clickedButton()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::clickedButton()
{
	QLineEdit *lEdit = new QLineEdit(ui->centralWidget);
	ui->centralWidget->layout()->addWidget(lEdit);
}